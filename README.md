# `fakesystemctl` - a hack to provide systemctl function

#### An interface among nosystemd inits, to be executed with sudo/doas, for services management and power actions, ONLY WHEN NECESSARY, i.e. when systemctl command can't be removed, although the services have been removed, like Webmin or gnome-shell power actions extensions.


![](https://imgur.com/j9BZqs6.png)

Available commands are:

- systemctl enable <service>
- systemctl start <service>
- systemctl restart <service>
- systemctl status <service>
- systemctl stop <service>
- systemctl status <service>
- systemctl disable <service>
- systemctl hibernate
- systemctl reboot
- systemctl poweroff
- systemctl suspend

-----

Other info:

- Licensed under GPL-2.0-only [https://spdx.org/licenses/GPL-2.0-only.html](https://spdx.org/licenses/GPL-2.0-only.html)

- Code complies with shellcheck [https://www.shellcheck.net](https://www.shellcheck.net)
- NOT for packaging purposes - I may be pulled by other packages PKGBUILDS, only when it is needed
- (if the above changes, this README and code, will be modified accortingly)
- Functions tested successfully with OpenRC, Runit, s6 and elogind environments
- Link it as systemctl to your /usr/bin folder with `sudo ln -S fakesystemctl /usr/bin/systemctl`
